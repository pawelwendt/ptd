import numpy as np
import matplotlib.pyplot as plt
import copy
import pylab
from enum import Enum

class EGenMode(Enum):
    SET = 0
    ADD = 1
    SUB = 2
    MUL = 3
    DIV = 4

class Signal:
    def __init__(self, T, fs):
        self.T = T
        self.fs = fs
        self.samples = int(fs * T)
        self.x = np.arange(0, self.T, 1.0 / fs)
        self.y = np.zeros(self.samples)
    def initializeX(startTime):
        interval = 1.0 / self.fs
        for i in range(0, self.samples):
            x[i] = startTime
            startTime += interval
    def generateSinusoid(self, A, f, phi, mode):
        if(mode == EGenMode.SET):
            self.y = [A * np.sin(2.0 * np.pi * f * self.x[i] + phi) for i in np.arange(self.samples)]
        elif(mode == EGenMode.ADD):
            self.y = [ self.y[i] + (A * np.sin(2.0 * np.pi * f * self.x[i] + phi)) for i in np.arange(self.samples)]
        elif(mode == EGenMode.SUB):
            self.y = [ self.y[i] - (A * np.sin(2.0 * np.pi * f * self.x[i] + phi)) for i in np.arange(self.samples)]
        elif(mode == EGenMode.MUL):
            self.y = [ self.y[i] * (A * np.sin(2.0 * np.pi * f * self.x[i] + phi)) for i in np.arange(self.samples)]
        elif(mode == EGenMode.DIV):
            self.y = [ self.y[i] / (A * np.sin(2.0 * np.pi * f * self.x[i] + phi)) for i in np.arange(self.samples)]
    def generateSweep(self, A, f0, f1, phi):
        k = (f1 - f0) / self.T
        self.y = [(A * np.sin(2.0 * np.pi * (k / 2.0 * self.x[i] + f0) * self.x[i] + phi)) for i in np.arange(self.samples)]
    def DFT(self):
        temp = Signal(self.T, self.fs)
        real = np.zeros(self.samples)
        imag = np.zeros(self.samples)
        
        for k in np.arange(self.samples):
            temp.x[k] = float(k * temp.fs / temp.samples)
            for n in np.arange(self.samples):
                real[k] += self.y[n] * np.cos((2.0 * np.pi * n * k) / self.samples)
                imag[k] += self.y[n] * -np.sin((2.0 * np.pi * n * k) / self.samples)
            temp.y[k] = np.sqrt(real[k] ** 2 + imag[k] ** 2)
        return temp
    def modulateAM(self, kam, fm, phim):
        self.y = [((1.0 * kam * self.y[i]) * np.sin(2.0 * np.pi * fm * self.x[i] + phim)) for i in np.arange(self.samples)]
    def modulatePM(self, kpm, fm, am):
        self.y = [(am * np.sin(2.0 * np. pi * fm * self.x[i] + kpm * self.y[i])) for i in np.arange(self.samples)]
    def modulateASK(self, A0, A1, length):
        temp = copy.deepcopy(self)
        tb = temp.T / length
        f = 1 / tb
        for i in np.arange(temp.samples):
            if(temp.y[i] == 0):
                temp.y[i] = A0 * np.sin(2.0 * np.pi * f * temp.x[i])
            else:
                temp.y[i] = A1 * np.sin(2.0 * np.pi * f * temp.x[i])
        return temp
    def modulateFSK(self, A, N, length):
        temp = copy.deepcopy(self)
        tb = temp.T / length
        f0 = (N + 1) / tb
        f1 = (N + 2) / tb
        for i in np.arange(temp.samples):
            if(temp.y[i] == 0):
                temp.y[i] = A * np.sin(2.0 * np.pi * f1 * temp.x[i])
            else:
                temp.y[i] = A * np.sin(2.0 * np.pi * f0 * temp.x[i])
        return temp
    def modulatePSK(self, A, N, phi0, phi1, length):
        temp = copy.deepcopy(self)
        tb = temp.T / length
        f = (1 / tb) * N
        f0 = ((N + 1) / tb)
        f1 = ((N + 2) / tb)
        for i in np.arange(temp.samples):
            if(temp.y[i] == 0):
                temp.y[i] = A * np.sin(2.0 * np.pi * f * temp.x[i] + phi0)
            else:
                temp.y[i] = A * np.sin(2.0 * np.pi * f * temp.x[i] + phi1)
        return temp
    def plotLines(self):
        plt.axhline(linewidth = 1, color = 'k')
        plt.grid()
        plt.plot(self.x, self.y)
        plt.show()
    def loadBinaryDataToSignal(self, data, length):
        tb = self.T / length #bit duration
        self.fs = 50 * (1 / tb)
        self.samples = int(self.fs * self.T)
        print("Loading bin data:\nbit duration:{0}\nsampling frequency:{1}\nsamples:{2}".format(tb, self.fs, self.samples))
        self.x = np.arange(0, self.T, 1.0 / self.fs)
        self.y = np.zeros(self.samples)
        samplePerBit = int(self.samples / length)
        
        for b in np.arange(length):
            for i in np.arange(samplePerBit):
                self.y[b * samplePerBit + i] = data[b]
def toBinary(string):
    return list("".join([format(ord(char),'#010b')[2:] for char in string]))

def plotHist(dft):
    markerline, stemlines, baseline = plt.stem(dft.x, dft.y, '-.')
    plt.setp(markerline, 'markerfacecolor', 'b')
    plt.setp(baseline, 'color', 'r', 'linewidth', 2)
    plt.show()

def plotModAndData(data, ask, fsk, psk):
    plt.subplot(4, 1, 1)
    plt.axis([0, data.T, -0.5, 1.5])
    plt.grid()
    plt.ylabel('data')
    plt.plot(data.x, data.y)
    
    plt.subplot(4, 1, 2)
    plt.axis([0, ask.T, -1.5, 1.5])
    plt.grid()
    plt.ylabel('ASK')
    plt.plot(ask.x, ask.y)

    plt.subplot(4, 1, 3)
    plt.axis([0, fsk.T, -1.5, 1.5])
    plt.grid()
    plt.ylabel('FSK')
    plt.plot(fsk.x, fsk.y)

    plt.subplot(4, 1, 4)
    plt.axis([0, psk.T, -1.5, 1.5])
    plt.grid()
    plt.ylabel('PSK')
    plt.plot(psk.x, psk.y)

    plt.subplots_adjust(left=0.04, bottom = 0.03, right = 0.99, top = 0.96, wspace = 0.29, hspace = 0.13)
    plt.show()
    
def main():
    signal = Signal(1.0, 1000.0)

main()
        

